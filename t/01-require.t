#!perl -T
use 5.10.0;
use strict;
use warnings;
use Test::More;
use FindBin;
use lib File::Spec->catfile( $FindBin::Bin, '..', 'lib' );

diag("Testing Git::Sugar $Git::Sugar::VERSION, Perl $], $^X");

plan tests => 5;

BEGIN {
    require_ok('Carp')            || print "Bail out!\n";
    require_ok('Git::Repository') || print "Bail out!\n";
    require_ok('Git::Sugar')      || print "Bail out!\n";
    require_ok('Ref::Util')       || print "Bail out!\n";
    require_ok('YAML')            || print "Bail out!\n";

}

