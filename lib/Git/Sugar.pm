package Git::Sugar;

=encoding UTF-8
 
=head1 NAME
 
Git::Sugar - Just some syntax sugar for Git::Repository
 
=head1 VERSION
 
version 0.0001

=cut

our $VERSION = '0.0001';    # VERSION

use utf8;
use strict;
use warnings FATAL => 'all';
use Git::Repository;
use Carp qw(cluck);
use Ref::Util qw /is_hashref/;
use vars qw($VERSION @EXPORT_OK);
use YAML qw(LoadFile);

require Exporter;
*import    = \&Exporter::import;
@EXPORT_OK = qw( checkout clean clone config fetch hash_object init last_author pull reset );

my $_hr_config;

=head1 EXPORT

=over 4

=item * checkout

=item * clean

=item * clone

=item * config

=item * fetch

=item * hash_object

=item * init

=item * last_author

=item * pull

=item * reset

=back

=head1 SUBROUTINES/METHODS

=head2 checkout

Checkout branch from branch name in given git repository.

=cut

sub checkout {
    return repository()->run( 'checkout', shift );
}

=head2 clean

Clean current branch in given git repository.

=cut

sub clean {
    return repository()->run( 'clean', shif || '-fd' );
}

=head2 clone

Clone repository form given param to repository given in config.

=cut

sub clone {
    Git::Repository->run( clone => shift, config()->{repository} );
}

=head2 config

Read configuration file from current path.

=cut

sub config {
    my $hr_data = shift;
    if ($hr_data) {
        cluck 'config data must be a hashref' if ( !is_hashref($hr_data) );
        $_hr_config = $hr_data;
    }
    return $_hr_config if $_hr_config;
    my $s_config_path;
    if ( -f 'config.yaml' ) {
        $s_config_path = 'config.yaml';
    }
    elsif ( -f 'config.yml' ) {
        $s_config_path = 'config.yml';
    }
    else {
        cluck "could not find a config.yml or config.yaml file";
    }
    $_hr_config = LoadFile($s_config_path)->{git_sugar};
    return $_hr_config;
}

=head2 fetch

Fetch given git repository.

=cut

sub fetch {
    return repository()->run('fetch');
}

=head2 init

Init own repository;

=cut

sub init {
    Git::Repository->run( init => config->{repository} );
}

=head2 last_author

Get the last author from file via git log.

=cut

sub last_author {
    return repository()->run( 'log', '--date=iso', '--pretty=format:"%cd - %ae : %s"', '-1', shift );
}

=head2 hash_object

Get the SHA1 for given file

=cut

sub hash_object {
    return repository()->run( 'hash-object', shift );
}

=head2 pull

Pull current branch in given git repository.

=cut

sub pull {
    return repository()->run('pull');
}

=head2 reset

Hard reset current branch in given git repository.

=cut

sub reset {
    return repository()->run( 'reset', shift || '--hard' );
}

=head2 repository

Returns the Git::Repository object.

=cut

sub repository {
    return Git::Repository->new( work_tree => config()->{repository}, { fatal => [ 1 .. 255 ], } );
}

# ABSTRACT: Just some syntax sugar for Git::Repository

1;

__END__

=pod
 
=head1 SYNOPSIS
 
Sample if no config file is given
    
    ...
    use Git::Sugar qw/config pull/;

    #set your config
    Git::Sugar::config({ repository => '/path/to/repository'});
    
    # Pull from given repository.
    pull;
    ...

=head1 DESCRIPTION
 
Just some syntax sugar for your Git::Repository applications.
 
=head1 CONFIGURATION
 
Configuration can be automatically parsed from a `config.yaml` or `config.yml`
file  in the current working directory, or it can be explicitly set with the
C<config> function:
 
    Git::Sugar::config({ repository => '/path/to/repository'});
 
If you want the config to be autoloaded from a yaml config file, just make sure
to put your config data under a top level C<git_sugar> key.
 
=head2 example
 
Here is a simple example. It defines one repository:
 
    git_sugar:
      repository: /path/to/repository

=head1 AUTHOR

Mario Zieschang, C<< <mziescha at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-app-namedquery-versioning at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=Git-Sugar>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.


=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Git::Sugar


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Git-Sugar>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Git-Sugar>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/Git-Sugar>

=item * Search CPAN

L<http://search.cpan.org/dist/Git-Sugar/>

=back

=head1 SEE ALSO
 
=over 4
 
=item L<Git>, L<Git::Repository>

=back

=head1 LICENSE AND COPYRIGHT

Copyright 2015 Mario Zieschang.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
